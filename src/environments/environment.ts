// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBN7DfcMg78P6fI9n7g6-U_LhQRwh9x6Lc',
    authDomain: 'formularz-b032c.firebaseapp.com',
    databaseURL: 'https://formularz-b032c.firebaseio.com',
    projectId: 'formularz-b032c',
    storageBucket: 'formularz-b032c.appspot.com',
    messagingSenderId: '1049317214961'
  }

};
