import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';


import { AngularFireModule } from 'angularfire2';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { WebsiteService } from './services/website-service';


import { AppComponent } from './app.component';
import { AlertComponent } from './modals/alert.component';
import { ConfirmComponent } from './modals/confirm.component';
import { FormComponent } from './components/form/form.component';
import { OfferComponent } from './components/offer/offer.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,    
    ConfirmComponent,    
    FormComponent,
    OfferComponent,    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: 'offer/:id',
        component: OfferComponent
      },
      {
        path: 'form',
        component: FormComponent
      }
    ]),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    BootstrapModalModule,    
  ],
  entryComponents: [
    AlertComponent,
    ConfirmComponent
  ],
  providers: [
    WebsiteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
