import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { WebsiteService } from '../services/website-service';

// to comment to check if work
export interface ConfirmModel {
  title: string;
  message: object;
}

@Component({
  selector: 'confirm',
  template: `<div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" (click)="close()" >&times;</button>
                     <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                   </div>
                   <div class="modal-body">
                    <li class ="keys" *ngFor = "let item of arr; let i = index" > <span (click) = "goOffer(i)">{{ item.recipient.name }}</span> <span (click)="showKey(item.$key)">klucz: {{item.$key}} </span></li>
                   </div>
                   <div class="modal-footer">
                   </div>
                 </div>
              </div>`,
  styles: [`
    .modal-dialog li {
      list-style: none;
     }
      .modal-dialog li:hover {
        background-color: #BBffBB;
        margin: 10px;
        position: relative;
        width: auto;
        cursor: pointer;
    }
      `]
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, object> implements ConfirmModel {
  arr: Array<object>;
  message: object;
  show: FirebaseListObservable<any>;
  title: string;

  constructor(dialogService: DialogService, private webService: WebsiteService) {
    super(dialogService);
  }
  goOffer(i) {
    this.result = this.arr[i];
    this.close();
  }

  showKey(key) {
    window.open('https://formularz-b032c.firebaseapp.com/offer/' + key);
  }

  ngOnInit() {
    this.show = this.webService.get();
    this.show.forEach(x => {
      this.arr = x;
    });
  }
}