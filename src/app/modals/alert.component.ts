import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';


export interface ConfirmModel {
  title: string;
  message: string;
  route: boolean;
  id: string;
}

@Component({
    selector: 'alert',
    template: `<div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" (click)="close()" >&times;</button>
                     <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                   </div>
                   <div class="modal-body">

                   <a routerLink="offer/{{ id }}" routerLinkActive="active" *ngIf = "route == true">Go to offer</a>
                   
                    {{ message }}
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default" (click)=" close()" >Ok</button>
                   </div>
                 </div>
              </div>`,              
})
export class AlertComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  route: boolean;
  id: string;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  goOffer(i) {
    this.result = true;
    this.close();
  }
  ngOnInit() {
  }

}











