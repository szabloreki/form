import { Injectable, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable  } from 'angularfire2/database';

@Injectable()
export class WebsiteService {
  items: FirebaseListObservable<any>;
  constructor( private db: AngularFireDatabase) {
    this.items = db.list('/offer', { preserveSnapshot: true });
  }
  addItem(object) {
    const doThis  = this.items.push(object);
    alert(`/offer/${doThis.key}`);
  }
  deleteItem(key: string) {
    this.items.remove(key);
  }
  get (): FirebaseListObservable<any[]> {
    return this.db.list('/offer');
  }
  updateItem(key: string, offer) {
    this.items.update(key, offer);
  }
}
