export interface Block  {
  edit: boolean;
  information: Array<string>;
  price: number;
  title: string;
  websiteEdit: string;
}