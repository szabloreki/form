import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable  } from 'angularfire2/database';
import { WebsiteService } from '../../services/website-service';
import {OnInit, Component, AfterViewInit, AfterViewChecked, ViewChild} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {
  private id: number;
  private show: FirebaseListObservable<any>;
  private watch: any;

  public doesntFinded: boolean;
  public finded: boolean;
  public loading: boolean;
  public offer: FirebaseObjectObservable<any>;

  constructor(private route: ActivatedRoute, private service: WebsiteService) { }
  ngOnInit() {
    this.show = this.service.get();
    this.watch = this.route.params
    .subscribe (params => {
        this.id = params['id'];
    });
    this.loading = true;
    this.show.forEach(x => {
      // cannot use map in this function
      for (const el of x){
        if (el.$key === this.id) {
          this.finded = true;
          this.offer = el;
          return;
        }
      }
      this.doesntFinded = true;
      this.loading = false;
    });
  }
}
