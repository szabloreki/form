import { AlertComponent } from '../../modals/alert.component';
import { AngularFireDatabase, FirebaseListObservable  } from 'angularfire2/database';
import { Block } from '../../models/offer'; 
import { OnInit, Component } from '@angular/core';
import { ConfirmComponent } from '../../modals/confirm.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { WebsiteService } from '../../services/website-service';

@Component({
  selector: 'app-root',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent {
  public block: Block;
  public template: boolean;
  public textArea: string;  
  public viev: boolean;
  public update: string;
  public export: any = {
    serviceProvider: '',
    recipient: {
      name: '',
      street: '',
      postcard: '',
      placement: '',
    },
    date: '',
    allFrame: []
  };
  private items: FirebaseListObservable<any>;

  constructor( private WebService: WebsiteService, private dialogService: DialogService) {
  }

  ngOnInit () {
    this.export = {
      serviceProvider : 'Michał Wójcik',
      date: '2017-06-20',
      recipient: {
        name: 'Dawid',
        street: 'ul. Widok 11',
        placement: 'Bielsko-Biała',
        postcard: '43-300'
      },
      allFrame: []
    };
    this.block = {
      title: 'Stworzenie strony internetowej opartej o CMS Wordpress:',
      websiteEdit: 'całkowita mozliwość edycji strony',
      information: ['sss'],
      price: 2000,
      edit: false
    };
    this.textArea = `-Stworzenie strony internetowej opartej o CMS Wordpress \n-Stworzenie strony internetowej opartej o Autorski CMS`;
    this.template = false;
    this.viev = false;
  }
  addBlock() {
    if (this.textArea === undefined) {
      return this.showAlert('Error', 'Information text Area is empty!', false, 'none');
    }
    if (this.textArea.indexOf('-') === -1 || this.textArea.length < 20) {
      return this.showAlert('Error' ,
       'Information must have at least  20 char and  1 "-" which is dividing selected category!',
        false, 'none');
    }
    const obj = {
      title: this.block.title,
      websiteEdit: this.block.websiteEdit,
      information: [],
      price: this.block.price,
      edit: this.block.edit
    };
    this.textArea.split('-').map(x => {
      obj.information.push(x);
    });
    obj.information.shift();
    this.export.allFrame.push(obj);
    console.log(this.export);
    return true;
  }
  addOffer () {
    if (this.export.allFrame.length === 0) {
      return this.showAlert('Error', 'You cannot add a offer witchout block!', false, 'none');
    }
    this.WebService.addItem(this.export);
  }
  showConfirm() {
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
        title: 'Choose an offer',
        message: {}})
        .subscribe((isConfirmed) => {
            if (isConfirmed) {
                this.export = isConfirmed;
                this.template = true;
                this.update = this.export.$key;
            } else {
            }
        });
    setTimeout(() => {
        disposable.unsubscribe();
    }, 10000);

  }
  edit(i) {
    this.export.allFrame.map( frame => { frame.edit = false; });
    this.export.allFrame[i].edit = true;
    this.block.title = this.export.allFrame[i].title;
    this.block.websiteEdit =  this.export.allFrame[i].websiteEdit;
    this.block.information = this.export.allFrame[i].information;
    this.block.price = this.export.allFrame[i].price;
    return true;
  }
  editSave(i) {
    const arr = [];
    this.export.allFrame[i].title = this.block.title;
    this.export.allFrame[i].websiteEdit = this.block.websiteEdit;
    this.textArea.split('-').map(string => {
      arr.push(string);
    });
    const newAr = arr.shift();
    this.export.allFrame[i].information = arr;
    this.export.allFrame[i].title = this.block.title;
    this.export.allFrame[i].edit = false;
    this.export.allFrame[i].price = this.block.price;
    return true;
  }
  vieving () {
    return this.viev = true;
  }
  deleteBlock (i) {
    this.export.allFrame.splice(i, 1);
    return true;
  }

  deleteOffer() {
    this.WebService.deleteItem(this.export.$key);
    this.template = false;
    return this.showAlert('Deleted', 'The offer was deleted', false, 'none');
  }
  updateOffer () {
    if ( this.export.allFrame.length <= 0) {
      return this.showAlert('Error', 'You cannot update the offer if you don\'t have a data with in!', false, 'none');
    }
    this.WebService.updateItem(this.update, this.export);
    return this.showAlert('Status', '', true,  this.export.$key);
  }
  showAlert(title: string, message: string, route: boolean, id: string) {
    const disposable = this.dialogService.addDialog(AlertComponent, {
        title: title,
        message: message,
        route: route,
        id: id
      })
        .subscribe((isConfirmed) => {});
        setTimeout(() => {
          disposable.unsubscribe();
      }, 10000);
  }
}
